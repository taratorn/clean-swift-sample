//
//  ITraffyAPIRequestAdepter.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//

import Foundation

protocol ITraffyAPIRequestAdepter {
	func getIssueList(offset: Int) -> URLRequest
}
