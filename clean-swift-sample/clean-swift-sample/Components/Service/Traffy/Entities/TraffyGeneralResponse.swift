//
//  TraffyGeneralResponse.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//

import Foundation

struct TraffyGeneralResponse<T: Decodable>: Decodable {
	var status: Bool?
	var total: Int?
	var results: [T]?
}
