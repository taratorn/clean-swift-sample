//
//  TraffyListWorkerSpy.swift
//  clean-swift-sampleTests
//
//  Created by Taratorn Deachboon on 18/8/2565 BE.
//

@testable import clean_swift_sample

class TraffyListWorkerSpy: ITraffyListWorker {
	
	var issueListStub: [TraffyIssueEntity] = []
	var issueList: [TraffyIssueEntity] {
		issueListStub
	}
	
	var getIssueListStub: (Result<[TraffyIssueEntity], Error>)?
	func getIssueList(completion: @escaping (Result<[TraffyIssueEntity], Error>) -> Void) {
		if let stub = getIssueListStub {
			completion(stub)
		}
	}
}
