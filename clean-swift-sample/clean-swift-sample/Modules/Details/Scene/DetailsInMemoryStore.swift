//
//  DetailsInMemoryStore.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 28/7/2565 BE.
//  Copyright (c) 2565 BE TMB. All rights reserved.
//

protocol DetailsInMemoryStoreProtocol {
	var issue: TraffyIssueEntity { get }
}

class DetailsInMemoryStore: DetailsInMemoryStoreProtocol {
	var issue: TraffyIssueEntity
	
	init(issue: TraffyIssueEntity) {
		self.issue = issue
	}
}
