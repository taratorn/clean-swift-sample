//
//  TraffyListInMemoryStore.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//  Copyright (c) 2565 BE TMB. All rights reserved.
//

protocol TraffyListInMemoryStoreProtocol {
	var issueList: [TraffyIssueEntity] { get }
	
	func save(issueList: [TraffyIssueEntity])
}

class TraffyListInMemoryStore {
	var issueList: [TraffyIssueEntity] = []
}

extension TraffyListInMemoryStore: TraffyListInMemoryStoreProtocol {
	func save(issueList: [TraffyIssueEntity]) {
		self.issueList = issueList
	}
}
