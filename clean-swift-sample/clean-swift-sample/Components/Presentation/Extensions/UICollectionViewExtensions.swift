//
//  UICollectionViewExtensions.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//

import UIKit

typealias CollectionViewDataInterface = UICollectionViewDelegate & UICollectionViewDataSource

extension UICollectionView {
	func dequeueReusableCell<T: UICollectionViewCell>(identifier: String, indexPath: IndexPath) -> T {
		guard let cell = self.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? T else {
			fatalError("Could not load reuseable cell")
		}
		
		return cell
	}
	
	func reload() {
		DispatchQueue.main.async {
			self.reloadData()
			self.layoutIfNeeded()
		}
	}
}
