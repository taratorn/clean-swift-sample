//
//  TraffyListPresenter.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//  Copyright (c) 2565 BE TMB. All rights reserved.
//

import UIKit

protocol TraffyListPresenterProtocol {
	func presentIssueList(response: GetIssueListUseCase.Response)
	func presentSelectedIssue(response: SelectedIssueUseCase.Response)
}

struct TraffyListPresenter {
	weak var viewController: TraffyListViewControllerProtocol?
	
	init(viewController: TraffyListViewControllerProtocol) {
		self.viewController = viewController
	}
}

// MARK: - ITraffyListPresenter
extension TraffyListPresenter: TraffyListPresenterProtocol {
	func presentIssueList(response: GetIssueListUseCase.Response) {
		let issueListViewModels: [ITraffyDataListViewModel] = response.issueList.map { TraffyDataListViewModel(issue: $0) }
		let viewModel = GetIssueListUseCase.ViewModel(issueListViewModels: issueListViewModels)
		viewController?.showIssueList(viewModel: viewModel)
	}
	
	func presentSelectedIssue(response: SelectedIssueUseCase.Response) {
		let viewModel = SelectedIssueUseCase.ViewModel(issue: response.issue)
		viewController?.showSelectedIssue(viewModel: viewModel)
	}
}
