//
//  IThemeAppearance.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 21/8/2565 BE.
//

import Foundation

protocol IThemeAppearance {
	var navigationAppearance: INavigationAppearance { get }
	
	func applyNavigationAppearance()
}
