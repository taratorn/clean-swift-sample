//
//  ITraffyAPIService.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//

import Foundation

protocol ITraffyAPIService {
	func getIssueList(offset: Int, completion: @escaping (Result<[TraffyIssueEntity], Error>) -> Void)
}
