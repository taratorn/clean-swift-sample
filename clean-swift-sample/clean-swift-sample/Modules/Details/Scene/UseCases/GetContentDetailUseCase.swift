//
//  GetContentDetailUseCase.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 2/8/2565 BE.
//

import MapKit

enum GetContentDetailUseCase {
	struct Request { }
	
	struct Response {
		let issue: TraffyIssueEntity
	}
	
	struct ViewModel {
		let coordinate: CLLocationCoordinate2D
		let zoomLevel: Int
		let annotation: MKPointAnnotation
		let shouldDisplayMap: Bool
		let title: String
		let imageURL: URL?
		let organize: String?
	}
}
