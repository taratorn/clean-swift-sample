//
//  TraffyListInteractorSpy.swift
//  clean-swift-sampleTests
//
//  Created by Taratorn Deachboon on 18/8/2565 BE.
//

@testable import clean_swift_sample

class TraffyListInteractorSpy: ITraffyListInteractor {
	
	var getIssueListIsCalled: Int = 0
	var getIssueListResult: GetIssueListUseCase.Request?
	func getIssueList(request: GetIssueListUseCase.Request) {
		getIssueListIsCalled += 1
		getIssueListResult = request
	}
	
	var selectedIssueIsCalled: Int = 0
	var selectedIssueResult: SelectedIssueUseCase.Request?
	func selectedIssue(request: SelectedIssueUseCase.Request) {
		selectedIssueIsCalled += 1
		selectedIssueResult = request
	}
}
