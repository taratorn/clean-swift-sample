//
//  TraffyDataListViewModel.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//

import Foundation

protocol ITraffyDataListViewModel {
	var title: String { get }
	var imageURL: URL? { get }
}

struct TraffyDataListViewModel: ITraffyDataListViewModel {
	var title: String
	var imageURL: URL?
	
	init(issue: TraffyIssueEntity) {
		self.title = issue.name ?? "unknown"
		self.imageURL = URL(string: issue.photo ?? "")
	}
}
