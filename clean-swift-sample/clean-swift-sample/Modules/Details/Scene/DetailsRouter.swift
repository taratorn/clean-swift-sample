//
//  DetailsRouter.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 28/7/2565 BE.
//  Copyright (c) 2565 BE TMB. All rights reserved.
//

import UIKit

protocol DetailsRouterProtocol {
	func backToPreviousScreen()
}

struct DetailsRouter {
	weak var viewController: DetailsViewController?
	
	init(viewController: DetailsViewController) {
		self.viewController = viewController
	}
}

// MARK: - IDetailsRouter
extension  DetailsRouter: DetailsRouterProtocol {
	func backToPreviousScreen() {
		viewController?.navigationController?.popViewController(animated: true)
	}
}
