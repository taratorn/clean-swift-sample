//
//  IMockBuilder.swift
//  clean-swift-sampleTests
//
//  Created by Taratorn Deachboon on 18/8/2565 BE.
//

import Foundation

protocol IMockBuilder {
	associatedtype Output
	static func build(parameter: [String: Any?]?) -> Output
}
