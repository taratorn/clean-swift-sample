//
//  TraffyListInteractor.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//  Copyright (c) 2565 BE TMB. All rights reserved.
//

import UIKit

protocol TraffyListInteractorProtocol {
	func getIssueList(request: GetIssueListUseCase.Request)
	func selectedIssue(request: SelectedIssueUseCase.Request)
}

struct TraffyListInteractor {
	var presenter: TraffyListPresenterProtocol
	var worker: TraffyListWorkerProtocol
	
	init(presenter: TraffyListPresenterProtocol,
		 worker: TraffyListWorkerProtocol) {
		self.presenter = presenter
		self.worker = worker
	}
}

// MARK: - ITraffyListInteractor
extension TraffyListInteractor: TraffyListInteractorProtocol {
	func getIssueList(request: GetIssueListUseCase.Request) {
		worker.getIssueList { result in
			switch result {
			case .success(let issueList):
				let response = GetIssueListUseCase.Response(issueList: issueList)
				presenter.presentIssueList(response: response)
			case .failure:
				// TODO: - Waitting for handle error
				break
			}
		}
	}
	
	func selectedIssue(request: SelectedIssueUseCase.Request) {
		let issue = worker.issueList[request.selectedIndex]
		let response = SelectedIssueUseCase.Response(issue: issue)
		presenter.presentSelectedIssue(response: response)
	}
}
