//
//  INetworkResponseHandler.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//

import Foundation

protocol INetworkResponseHandler {
	func handleResponse(data: Data?, response: URLResponse?, error: Error?, completion: @escaping (Result<Data?, Error>) -> Void)
}
