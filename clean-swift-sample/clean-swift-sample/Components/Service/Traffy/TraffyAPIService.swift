//
//  TraffyAPIService.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//

import Foundation

struct TraffyAPIService {
	var client: INetworkClient
	var requestAdapter: ITraffyAPIRequestAdepter
	var responseAdapter: ITraffyAPIResponseAdapter
	
	init(client: INetworkClient,
		 requestAdapter: ITraffyAPIRequestAdepter,
		 responseAdapter: ITraffyAPIResponseAdapter) {
		self.client = client
		self.requestAdapter = requestAdapter
		self.responseAdapter = responseAdapter
	}
}

// MARK: - ITraffyAPIService
extension TraffyAPIService: ITraffyAPIService {
	func getIssueList(offset: Int, completion: @escaping (Result<[TraffyIssueEntity], Error>) -> Void) {
		let request = requestAdapter.getIssueList(offset: offset)
		client.request(request) { result in
			switch result {
			case .success(let data):
				responseAdapter.getIssueList(data: data, completion: completion)
			case .failure(let error):
				completion(.failure(error))
			}
		}
	}
}
