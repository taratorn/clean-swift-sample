//
//  CollectionExtensions.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 2/8/2565 BE.
//

extension Collection {
	subscript (safe index: Index) -> Element? {
		return indices.contains(index) ? self[index] : nil
	}
}
