//
//  TraffyAPIResponseAdapter.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//

import Foundation

struct TraffyAPIResponseAdapter {
	var serializerService: ISerializerService
	
	init(serializerService: ISerializerService) {
		self.serializerService = serializerService
	}
}

extension TraffyAPIResponseAdapter: ITraffyAPIResponseAdapter {
	func getIssueList(data: Data?, completion: (Result<[TraffyIssueEntity], Error>) -> Void) {
		do {
			let generalResponse: TraffyGeneralResponse<TraffyIssueEntity> = try serializerService.serialize(data: data)
			guard let results = generalResponse.results, !results.isEmpty else {
				completion(.success([]))
				return
			}
			
			completion(.success(results))
		} catch {
			completion(.failure(error))
		}
	}
}
