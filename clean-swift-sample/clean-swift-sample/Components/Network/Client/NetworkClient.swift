//
//  NetworkClient.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//

import Foundation

struct NetworkClient {
	
	var session: URLSession
	var responseHandler: INetworkResponseHandler
	
	init(session: URLSession, responseHandler: INetworkResponseHandler) {
		self.session = session
		self.responseHandler = responseHandler
	}
}

extension NetworkClient: INetworkClient {
	func request(_ request: URLRequest, completion: @escaping (Result<Data?, Error>) -> Void) {
		session.dataTask(with: request) { data, response, error in
			responseHandler.handleResponse(data: data, response: response, error: error, completion: completion)
		}.resume()
	}
}
