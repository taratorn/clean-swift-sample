//
//  TraffyListPresenterSpy.swift
//  clean-swift-sampleTests
//
//  Created by Taratorn Deachboon on 18/8/2565 BE.
//

@testable import clean_swift_sample

class TraffyListPresenterSpy: ITraffyListPresenter {
	
	var presentIssueListIsCalled: Int = 0
	var presentIssueListResult: GetIssueListUseCase.Response?
	func presentIssueList(response: GetIssueListUseCase.Response) {
		presentIssueListIsCalled += 1
		presentIssueListResult = response
	}
	
	var presentSelectedIssueIsCalled: Int = 0
	var presentSelectedIssueResult: SelectedIssueUseCase.Response?
	func presentSelectedIssue(response: SelectedIssueUseCase.Response) {
		presentSelectedIssueIsCalled += 1
		presentSelectedIssueResult = response
	}
}
