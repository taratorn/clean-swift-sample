//
//  GetIssueListUseCase.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 2/8/2565 BE.
//

import Foundation

enum GetIssueListUseCase {
	struct Request { }
	
	struct Response {
		let issueList: [TraffyIssueEntity]
	}
	
	struct ViewModel {
		let issueListViewModels: [ITraffyDataListViewModel]
	}
}
