//
//  DetailsPresenter.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 28/7/2565 BE.
//  Copyright (c) 2565 BE TMB. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

private enum Constants {
	static let defaultLatitude: Double = 13.7464
	static let defaultLongitude: Double = 100.5347
	static let defaultZoomLevel: Int = 16
}

protocol DetailsPresenterProtocol {
	func presentDetails(response: GetContentDetailUseCase.Response)
}

struct DetailsPresenter {
	weak var viewController: DetailsViewControllerProtocol?
	
	init(viewController: DetailsViewControllerProtocol) {
		self.viewController = viewController
	}
}

// MARK: - IDetailsPresenter
extension DetailsPresenter: DetailsPresenterProtocol {
	func presentDetails(response: GetContentDetailUseCase.Response) {
		var latitude = Constants.defaultLatitude
		var longitude = Constants.defaultLongitude
		var shouldDisplayMap: Bool = false
		
		if let long = response.issue.coordinators?[safe: 0],
		   let lat = response.issue.coordinators?[safe: 1] {
			latitude = lat
			longitude = long
			shouldDisplayMap = true
		}
		
		let coordinator = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
		let annotation = MKPointAnnotation()
		annotation.coordinate = coordinator
		annotation.title = response.issue.name
		
		let title = response.issue.name ?? "-"
		let org = response.issue.org ?? "-"
		let imageURL = URL(string: response.issue.photo ?? "")
		
		let viewModel = GetContentDetailUseCase.ViewModel(coordinate: coordinator,
														  zoomLevel: Constants.defaultZoomLevel,
														  annotation: annotation,
														  shouldDisplayMap: shouldDisplayMap,
														  title: title, imageURL: imageURL,
														  organize: org)
		viewController?.showDetail(viewModel: viewModel)
	}
}
