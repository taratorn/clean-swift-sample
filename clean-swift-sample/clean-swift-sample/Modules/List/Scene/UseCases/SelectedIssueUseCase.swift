//
//  SelectedItemsUseCase.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 2/8/2565 BE.
//

import Foundation

enum SelectedIssueUseCase {
	struct Request {
		let selectedIndex: Int
	}
	
	struct Response {
		let issue: TraffyIssueEntity
	}
	
	struct ViewModel {
		let issue: TraffyIssueEntity
	}
}
