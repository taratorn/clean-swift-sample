//
//  INetworkConfiguration.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 2/8/2565 BE.
//

import Foundation

protocol INetworkConfiguration {
	var endpoint: URL { get }
}
