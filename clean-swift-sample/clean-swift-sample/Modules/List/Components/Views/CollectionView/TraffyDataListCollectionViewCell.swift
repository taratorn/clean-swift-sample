//
//  TraffyDataListCollectionViewCell.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//

import UIKit
private enum Constants {
	static let fontSize: CGFloat = 20
}

class TraffyDataListCollectionViewCell: UICollectionViewCell {
	
	@IBOutlet weak var issueImageView: UIImageView!
	@IBOutlet weak var issueTitleLabel: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		setupUI()
	}
	
	override func prepareForReuse() {
		super.prepareForReuse()
		issueTitleLabel.text = nil
		issueImageView.clearImage()
	}
}

// MARK: - Configure
extension TraffyDataListCollectionViewCell {
	func configure(viewModel: ITraffyDataListViewModel) {
		issueTitleLabel.text = viewModel.title
		if let url = viewModel.imageURL {
			issueImageView.loadImage(from: url)
		} else {
			issueImageView.clearImage()
		}
	}
}

// MARK: - Private
private extension TraffyDataListCollectionViewCell {
	func setupUI() {
		issueTitleLabel.font = UIFont.systemFont(ofSize: Constants.fontSize)
		issueTitleLabel.textColor = .black
	}
}
