//
//  ThemeAppearance.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 21/8/2565 BE.
//

import UIKit

struct ThemeAppearance {
	var navigationAppearance: INavigationAppearance
	
	init(navigationAppearance: INavigationAppearance) {
		self.navigationAppearance = navigationAppearance
	}
}

extension ThemeAppearance: IThemeAppearance {
	func applyNavigationAppearance() {
		UINavigationBar.appearance().barTintColor = navigationAppearance.barColor
		UINavigationBar.appearance().tintColor = navigationAppearance.tintColor
		UINavigationBar.appearance().titleTextAttributes = [
			NSAttributedString.Key.foregroundColor : navigationAppearance.titleColor,
			NSAttributedString.Key.font: navigationAppearance.font
		]
	}
}
