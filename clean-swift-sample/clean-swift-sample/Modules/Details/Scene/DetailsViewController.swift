//
//  DetailsViewController.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 28/7/2565 BE.
//  Copyright (c) 2565 BE TMB. All rights reserved.
//

import UIKit
import MapKit

protocol DetailsViewControllerProtocol: AnyObject {
	func showDetail(viewModel: GetContentDetailUseCase.ViewModel)
}

class DetailsViewController: UIViewController {
	
	// MARK: - Views
	@IBOutlet weak var mapView: MKMapView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var detailLabel: UILabel!
	@IBOutlet weak var mapContainerView: UIView!
	@IBOutlet weak var imageContainerView: UIView!
	@IBOutlet weak var detailContainerView: UIView!
	@IBOutlet weak var issueImageView: UIImageView!
	
	var interactor: DetailsInteractorProtocol!
	var router: DetailsRouterProtocol!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setupUI()
		getDetails()
	}
}

// MARK: - Interactor
extension DetailsViewController {
	func getDetails() {
		let request = GetContentDetailUseCase.Request()
		interactor.getDetails(request: request)
	}
}

// MARK: - IDetailsViewController
extension DetailsViewController: DetailsViewControllerProtocol {
	func showDetail(viewModel: GetContentDetailUseCase.ViewModel) {
		titleLabel.text = viewModel.title
		detailLabel.text = viewModel.organize
		
		mapContainerView.isHidden = !viewModel.shouldDisplayMap
		mapView.setCenterCoordinate(viewModel.coordinate, withZoomLevel: 16, animated: true)
		mapView.addAnnotation(viewModel.annotation)
		
		if let imageURL = viewModel.imageURL {
			imageContainerView.isHidden = false
			issueImageView.loadImage(from: imageURL)
		} else {
			imageContainerView.isHidden = true
		}
	}
}

// MARK: - Private
private extension DetailsViewController {
	func setupUI() {
		navigationController?.setNavigationBarHidden(false, animated: false)
	}
}
