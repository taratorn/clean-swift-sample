//
//  SerializerService.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//

import Foundation

struct SerializerService { }

extension SerializerService: ISerializerService {
	func serialize<T: Decodable>(data: Data?) throws -> T {
		guard let data = data else {
			throw SerializerException.emptyResponse
		}
		
		do {
			let decoder = JSONDecoder()
			let result = try decoder.decode(T.self, from: data)
			return result
		} catch {
			let exception = SerializerException.serializeError(error)
			throw exception
		}
	}
}
