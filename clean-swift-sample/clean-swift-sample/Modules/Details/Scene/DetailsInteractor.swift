//
//  DetailsInteractor.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 28/7/2565 BE.
//  Copyright (c) 2565 BE TMB. All rights reserved.
//

import UIKit

protocol DetailsInteractorProtocol {
	func getDetails(request: GetContentDetailUseCase.Request)
}

struct DetailsInteractor {
	var presenter: DetailsPresenterProtocol
	var worker: DetailsWorkerProtocol
	
	init(presenter: DetailsPresenterProtocol, worker: DetailsWorkerProtocol) {
		self.presenter = presenter
		self.worker = worker
	}
}

// MARK: - IDetailsInteractor
extension DetailsInteractor: DetailsInteractorProtocol {
	func getDetails(request: GetContentDetailUseCase.Request) {
		let issue = worker.issue
		let response = GetContentDetailUseCase.Response(issue: issue)
		presenter.presentDetails(response: response)
	}
}
