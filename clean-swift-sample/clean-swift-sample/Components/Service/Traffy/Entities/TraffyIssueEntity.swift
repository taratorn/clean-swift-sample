//
//  TraffyIssueEntity.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//

import Foundation

struct TraffyIssueEntity: Codable {
	var name: String?
	var zoneEn: String?
	var zoneTh: String?
	var coordinators: [Double]?
	var type: String?
	var org: String?
	var floors: String?
	var photo: String?
	var timestamp: String?
	
	enum CodingKeys: String, CodingKey {
		case name
		case zoneEn = "zone"
		case coordinators = "coords"
		case type
		case org
		case floors
		case photo
		case timestamp
		case zoneTh = "zone_th"
	}
}

extension TraffyIssueEntity: Equatable { }
