//
//  TraffyAPIRequestAdapter.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//

import Foundation

struct TraffyAPIRequestAdapter {
	
	var configuration: INetworkConfiguration
	
	init(configuration: INetworkConfiguration) {
		self.configuration = configuration
	}
}

extension TraffyAPIRequestAdapter: ITraffyAPIRequestAdepter {
	func getIssueList(offset: Int) -> URLRequest {
		let url = configuration.endpoint.appendingPathComponent("ud/search")
		var components = URLComponents(string: url.absoluteString)
		components?.queryItems = [
			URLQueryItem(name: "offset", value: String(offset))
		]
		
		let request = URLRequest(url: components?.url ?? url)
		return request
	}
}
