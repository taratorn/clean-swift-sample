//
//  TraffyListBuilder.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//  Copyright (c) 2565 BE TMB. All rights reserved.
//

import UIKit

private enum Constants {
	static let storyboardName: String = "TraffyList"
}

struct TraffyListBuilder { }

extension TraffyListBuilder: SceneBuildable {
	static func build(parameter: [String : Any?]?) throws -> UIViewController {
		let bundle = Bundle(for: TraffyListViewController.self)
		let identifier = String(describing: TraffyListViewController.self)
		
		let storyboard = UIStoryboard(name: Constants.storyboardName, bundle: bundle)
		guard let viewController = storyboard.instantiateViewController(withIdentifier: identifier) as? TraffyListViewController else {
			throw SceneBuilderException.moduleNotFound
		}
		
		let inMemoryStore = TraffyListInMemoryStore()
		let worker = TraffyListWorker(inMemoryStore: inMemoryStore,
									  traffyAPIService: DI.shared.traffyAPIService)
		let presenter = TraffyListPresenter(viewController: viewController)
		let interactor = TraffyListInteractor(presenter: presenter, worker: worker)
		let router = TraffyListRouter(viewController: viewController)
		
		viewController.interactor = interactor
		viewController.router = router
		
		let navigation = UINavigationController(rootViewController: viewController)
		return navigation
	}
}
