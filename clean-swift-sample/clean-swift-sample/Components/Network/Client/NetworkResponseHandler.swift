//
//  NetworkResponseHandler.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//

import Foundation


struct NetworkResponseHandler { }

extension NetworkResponseHandler: INetworkResponseHandler {
	func handleResponse(data: Data?, response: URLResponse?, error: Error?, completion: @escaping (Result<Data?, Error>) -> Void) {
		
		if let httpResponse = response as? HTTPURLResponse {
			switch httpResponse.statusCode {
			case 200..<299:
				completion(.success(data))
			default:
				let error = NetworkAPIError(statusCode: httpResponse.statusCode)
				completion(.failure(error))
			}
		} else {
			if let errorResponse = error as NSError? {
				if errorResponse.code == NSURLErrorTimedOut {
					completion(.failure(NetworkConnectionTimeout()))
				} else {
					completion(.failure(errorResponse))
				}
			}
		}
	}
}
