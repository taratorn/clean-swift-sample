//
//  TraffyListRouter.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//  Copyright (c) 2565 BE TMB. All rights reserved.
//

import UIKit

protocol TraffyListRouterProtocol {
	func openDetail(issue: TraffyIssueEntity)
}

struct TraffyListRouter {
	weak var viewController: UIViewController?
	
	init(viewController: UIViewController) {
		self.viewController = viewController
	}
}

// MARK: - ITraffyListRouter
extension  TraffyListRouter: TraffyListRouterProtocol {
	func openDetail(issue: TraffyIssueEntity) {
		let parameter: [String: Any] = ["issue": issue]
		guard let scene = try? DetailsBuilder.build(parameter: parameter) else { return }
		viewController?.navigationController?.pushViewController(scene, animated: true)
	}
}
