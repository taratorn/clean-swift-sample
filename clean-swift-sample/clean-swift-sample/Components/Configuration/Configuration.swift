//
//  Configuration.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 2/8/2565 BE.
//

import Foundation

enum Configuration {
	
	enum Keys {
		static let traffyAPIEndpoint: String = "TRAFFY_API_ENDPOINT"
	}
	
	static func value<T>(for key: String) -> T where T: LosslessStringConvertible {
		guard let object = Bundle.main.object(forInfoDictionaryKey: key) else {
			fatalError("missing key \(key)")
		}

		switch object {
		case let value as T:
			return value
		case let string as String:
			guard let value = T(string) else { fallthrough }
			return value
		default: fatalError("Invalid value for \(key)")
		}
	}
}
