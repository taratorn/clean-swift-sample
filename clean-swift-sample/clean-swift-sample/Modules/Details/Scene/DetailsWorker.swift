//
//  DetailsWorker.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 28/7/2565 BE.
//  Copyright (c) 2565 BE TMB. All rights reserved.
//

protocol DetailsWorkerProtocol {
	var issue: TraffyIssueEntity { get }
}

struct DetailsWorker {
	var inMemoryStore: DetailsInMemoryStoreProtocol
	
	init(inMemoryStore: DetailsInMemoryStoreProtocol) {
		self.inMemoryStore = inMemoryStore
	}
}

extension DetailsWorker: DetailsWorkerProtocol {
	var issue: TraffyIssueEntity {
		inMemoryStore.issue
	}
}
