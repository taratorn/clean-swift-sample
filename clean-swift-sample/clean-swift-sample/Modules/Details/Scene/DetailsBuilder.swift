//
//  DetailsBuilder.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 28/7/2565 BE.
//  Copyright (c) 2565 BE TMB. All rights reserved.
//

import UIKit

private enum Constants {
	static let storyboardName: String = "Details"
}

struct DetailsBuilder { }

extension DetailsBuilder: SceneBuildable {
	static func build(parameter: [String : Any?]?) throws -> UIViewController {
		let bundle = Bundle(for: DetailsViewController.self)
		let identifier = String(describing: DetailsViewController.self)
		
		let storyboard = UIStoryboard(name: Constants.storyboardName, bundle: bundle)
		guard let viewController = storyboard.instantiateViewController(withIdentifier: identifier) as? DetailsViewController else {
			throw SceneBuilderException.moduleNotFound
		}
		
		guard let issue = parameter?["issue"] as? TraffyIssueEntity else {
			throw SceneBuilderException.parameterNotFound
		}
		
		let inMemoryStore = DetailsInMemoryStore(issue: issue)
		let worker = DetailsWorker(inMemoryStore: inMemoryStore)
		let presenter = DetailsPresenter(viewController: viewController)
		let interactor = DetailsInteractor(presenter: presenter, worker: worker)
		let router = DetailsRouter(viewController: viewController)
		
		viewController.interactor = interactor
		viewController.router = router
		
		return viewController
	}
}
