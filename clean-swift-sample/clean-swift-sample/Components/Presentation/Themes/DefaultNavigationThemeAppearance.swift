//
//  DefaultNavigationThemeAppearance.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 21/8/2565 BE.
//

import UIKit

private enum Constants {
	static let defaultFontSize: CGFloat = 20
}

struct DefaultNavigationThemeAppearance { }

extension DefaultNavigationThemeAppearance: INavigationAppearance {
	var tintColor: UIColor {
		.black
	}
	
	var barColor: UIColor {
		.white
	}
	
	var font: UIFont {
		UIFont.systemFont(ofSize: Constants.defaultFontSize)
	}
	
	var titleColor: UIColor {
		.black
	}
}
