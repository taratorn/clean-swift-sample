//
//  ISerializerService.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//

import Foundation

enum SerializerException: Error {
	case emptyResponse
	case serializeError(_ error: Error)
}

protocol ISerializerService {
	func serialize<T: Decodable>(data: Data?) throws -> T
}
