//
//  ITraffyAPIResponseAdapter.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//

import Foundation

protocol ITraffyAPIResponseAdapter {
	func getIssueList(data: Data?, completion: (Result<[TraffyIssueEntity], Error>) -> Void)
}
