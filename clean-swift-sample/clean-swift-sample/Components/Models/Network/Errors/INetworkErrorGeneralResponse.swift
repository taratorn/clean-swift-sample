//
//  INetworkErrorGeneralResponse.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 21/8/2565 BE.
//

import Foundation

protocol INetworkError: Error {
	var statusCode: Int { get }
	var message: String? { get }
}
