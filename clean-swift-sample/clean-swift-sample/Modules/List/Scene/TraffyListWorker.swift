//
//  TraffyListWorker.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//  Copyright (c) 2565 BE TMB. All rights reserved.
//

private enum Constants {
	static let defaultOffset: Int = 20
}

protocol TraffyListWorkerProtocol {
	var issueList: [TraffyIssueEntity] { get }
	
	func getIssueList(completion: @escaping (Result<[TraffyIssueEntity], Error>) -> Void)
}

struct TraffyListWorker {
	var inMemoryStore: TraffyListInMemoryStoreProtocol
	var traffyAPIService: ITraffyAPIService
	
	init(inMemoryStore: TraffyListInMemoryStoreProtocol, traffyAPIService: ITraffyAPIService) {
		self.inMemoryStore = inMemoryStore
		self.traffyAPIService = traffyAPIService
	}
}

extension TraffyListWorker: TraffyListWorkerProtocol {
	var issueList: [TraffyIssueEntity] {
		inMemoryStore.issueList
	}
	
	func getIssueList(completion: @escaping (Result<[TraffyIssueEntity], Error>) -> Void) {
		traffyAPIService.getIssueList(offset: Constants.defaultOffset) { result in
			switch result {
			case .success(let issueList):
				inMemoryStore.save(issueList: issueList)
				completion(.success(issueList))
			case .failure(let error):
				completion(.failure(error))
			}
		}
	}
}
