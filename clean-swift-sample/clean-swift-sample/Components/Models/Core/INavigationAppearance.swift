//
//  INavigationAppearance.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 21/8/2565 BE.
//

import UIKit

protocol INavigationAppearance {
	var tintColor: UIColor { get }
	var barColor: UIColor { get }
	var titleColor: UIColor { get }
	var font: UIFont { get }
}
