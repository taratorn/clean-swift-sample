//
//  INetworkClient.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//

import Foundation

protocol INetworkClient {
	func request(_ request: URLRequest, completion: @escaping (Result<Data?, Error>) -> Void)
}
