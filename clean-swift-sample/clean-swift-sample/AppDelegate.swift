//
//  AppDelegate.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 28/7/2565 BE.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

	// MARK: - Internal
	var window: UIWindow?
	
	// MARK: - Private
	private var themeAppearance: IThemeAppearance!

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		initiateDependencies()
		initiateScreen()
		applyThemeAppearance()
		return true
	}
}

// MARK: - Private

private extension AppDelegate {
	func initiateScreen() {
		do {
			window = UIWindow(frame: UIScreen.main.bounds)
			window?.rootViewController = try TraffyListBuilder.build(parameter: nil)
			window?.makeKeyAndVisible()
		} catch {
			print("Error : \(error)")
		}
	}
	
	func initiateDependencies() {
		self.themeAppearance = DI.shared.themeAppearance
	}
	
	func applyThemeAppearance() {
		self.themeAppearance.applyNavigationAppearance()
	}
}
