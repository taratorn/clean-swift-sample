//
//  DI.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//

import Foundation

struct DI {
	
	// MARK: - Singletion
	static let shared: DI = DI()
	
	// MARK: - Presentation
	var themeAppearance: IThemeAppearance
	
	// MARK: - Common
	var serializerService: ISerializerService
	
	// MARK: - Network
	var session: URLSession
	var networkClient: INetworkClient
	var networkResponseHandler: INetworkResponseHandler
	
	// MARK: - APIService
	var traffyAPIService: ITraffyAPIService
	
	init() {
		
		// MARK: - Presentation
		let navigationAppearance = DefaultNavigationThemeAppearance()
		themeAppearance = ThemeAppearance(navigationAppearance: navigationAppearance)
		
		// MARK: - Common
		serializerService = SerializerService()
		
		// MARK: - Network
		let configuration = URLSessionConfiguration.default
		session = URLSession(configuration: configuration)
		networkResponseHandler = NetworkResponseHandler()
		networkClient = NetworkClient(session: session, responseHandler: networkResponseHandler)
		
		// MARK: - API
		let traffyConfiguration = TraffyAPIConfiguration()
		let traffyAPIRequestAdapter = TraffyAPIRequestAdapter(configuration: traffyConfiguration)
		let traffyAPIResponseAdapter = TraffyAPIResponseAdapter(serializerService: serializerService)
		traffyAPIService = TraffyAPIService(client: networkClient,
												requestAdapter: traffyAPIRequestAdapter,
												responseAdapter: traffyAPIResponseAdapter)
	}
}
