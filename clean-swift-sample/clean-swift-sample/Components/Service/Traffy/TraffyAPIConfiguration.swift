//
//  TraffyAPIConfiguration.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 2/8/2565 BE.
//

import Foundation

struct TraffyAPIConfiguration { }

extension TraffyAPIConfiguration: INetworkConfiguration {
	var endpoint: URL {
		let key = Configuration.Keys.traffyAPIEndpoint
		return URL(string: Configuration.value(for: key))!
	}
}
