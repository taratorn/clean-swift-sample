//
//  UIImageViewExtensions.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//

import UIKit

extension UIImageView {
	func loadImage(from url: URL) {
		DI.shared.session.dataTask(with: url) { data, response, error in
			DispatchQueue.main.async() { [weak self] in
				if let data = data, let image = UIImage(data: data) {
					self?.image = image
				} else {
					self?.image = nil
				}
			}
		}.resume()
	}
	
	func clearImage() {
		self.image = nil
	}
}
