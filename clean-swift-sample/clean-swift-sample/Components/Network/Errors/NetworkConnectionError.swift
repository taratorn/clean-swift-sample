//
//  NetworkConnectionError.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//

import Foundation

struct NetworkAPIError: INetworkError {
	var statusCode: Int
	var message: String?
	
	init(statusCode: Int, message: String? = nil) {
		self.statusCode = statusCode
		self.message = message
	}
}

struct NetworkConnectionTimeout: INetworkError {
	var statusCode: Int
	var message: String?
	
	init() {
		self.statusCode = NSURLErrorTimedOut
		self.message = "Session Timeout"
	}
}
