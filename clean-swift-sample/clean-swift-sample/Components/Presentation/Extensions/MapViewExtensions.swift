//
//  MapViewExtensions.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 5/8/2565 BE.
//

import UIKit
import MapKit

private enum Constants {
	static let MERCATOR_OFFSET: Double = 268435456
	static let MERCATOR_RADIUS: Double = 85445659.44705395
}

extension MKMapView {
	open var currentZoomLevel: Int {
		let maxZoom: Double = 24
		let zoomScale = visibleMapRect.size.width / Double(frame.size.width)
		let zoomExponent = log2(zoomScale)
		return Int(maxZoom - ceil(zoomExponent))
	}
	
	open func setCenterCoordinate(_ centerCoordinate: CLLocationCoordinate2D,
								  withZoomLevel zoomLevel: Int,
								  animated: Bool) {
		let minZoomLevel = min(zoomLevel, 28)
		
		let span = coordinateSpan(centerCoordinate, andZoomLevel: minZoomLevel)
		let region = MKCoordinateRegion(center: centerCoordinate, span: span)
		
		setRegion(region, animated: animated)
	}
}

struct PixelSpace {
	public var x: Double
	public var y: Double
}

// MARK: - Private
private extension MKMapView {
	func coordinateSpan(_ centerCoordinate: CLLocationCoordinate2D, andZoomLevel zoomLevel: Int) -> MKCoordinateSpan {
		let space = pixelSpace(fromLongitude: centerCoordinate.longitude, withLatitude: centerCoordinate.latitude)
		
		let zoomExponent = 20 - zoomLevel
		let zoomScale = pow(2.0, Double(zoomExponent))
		
		let mapSizeInPixels = self.bounds.size
		let scaledMapWidth = Double(mapSizeInPixels.width) * zoomScale
		let scaledMapHeight = Double(mapSizeInPixels.height) * zoomScale
		
		let topLeftPixelX = space.x - (scaledMapWidth / 2)
		let topLeftPixelY = space.y - (scaledMapHeight / 2)
		
		var minSpace = space
		minSpace.x = topLeftPixelX
		minSpace.y = topLeftPixelY
		
		var maxSpace = space
		maxSpace.x += scaledMapWidth
		maxSpace.y += scaledMapHeight
		
		let minLongitude = coordinate(fromPixelSpace: minSpace).longitude
		let maxLongitude = coordinate(fromPixelSpace: maxSpace).longitude
		let longitudeDelta = maxLongitude - minLongitude
		
		let minLatitude = coordinate(fromPixelSpace: minSpace).latitude
		let maxLatitude = coordinate(fromPixelSpace: maxSpace).latitude
		let latitudeDelta = -1 * (maxLatitude - minLatitude)
		
		return MKCoordinateSpan(latitudeDelta: latitudeDelta, longitudeDelta: longitudeDelta)
	}
	
	func pixelSpace(fromLongitude longitude: Double, withLatitude latitude: Double) -> PixelSpace {
		let x = round(Constants.MERCATOR_OFFSET + Constants.MERCATOR_RADIUS * longitude * Double.pi / 180.0)
		let y = round(Constants.MERCATOR_OFFSET - Constants.MERCATOR_RADIUS * log((1 + sin(latitude * Double.pi / 180.0)) / (1 - sin(latitude * Double.pi / 180.0))) / 2.0)
		return PixelSpace(x: x, y: y)
	}
	
	func coordinate(fromPixelSpace pixelSpace: PixelSpace) -> CLLocationCoordinate2D {
		let longitude = ((round(pixelSpace.x) - Constants.MERCATOR_OFFSET) / Constants.MERCATOR_RADIUS) * 180.0 / Double.pi
		let latitude = (Double.pi / 2.0 - 2.0 * atan(exp((round(pixelSpace.y) - Constants.MERCATOR_OFFSET) / Constants.MERCATOR_RADIUS))) * 180.0 / Double.pi
		return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
	}
}
