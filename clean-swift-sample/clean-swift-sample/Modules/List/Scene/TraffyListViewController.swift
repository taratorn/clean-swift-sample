//
//  TraffyListViewController.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 1/8/2565 BE.
//  Copyright (c) 2565 BE TMB. All rights reserved.
//

import UIKit

private enum Constants {
	static let lineSpcaing: CGFloat = 8
	static let numberOfColumn: CGFloat = 2
	static let horizontalPadding: CGFloat = 8
	static let contentHeight: CGFloat = 36
}

protocol TraffyListViewControllerProtocol: AnyObject {
	func showIssueList(viewModel: GetIssueListUseCase.ViewModel)
	func showSelectedIssue(viewModel: SelectedIssueUseCase.ViewModel)
}

class TraffyListViewController: UIViewController {
	
	// MARK: - Views
	@IBOutlet weak var collectionView: UICollectionView!
	
	var interactor: TraffyListInteractorProtocol!
	var router: TraffyListRouterProtocol!
	
	// MARK: - Private
	private var viewModels: [ITraffyDataListViewModel] = []
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setupUI()
		setupCollectionView()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		getIssueList()
	}
}

// MARK: - Interactor
extension TraffyListViewController {
	func getIssueList() {
		let request = GetIssueListUseCase.Request()
		interactor.getIssueList(request: request)
	}
	
	func selectIssue(selectedIndex: Int) {
		let request = SelectedIssueUseCase.Request(selectedIndex: selectedIndex)
		interactor.selectedIssue(request: request)
	}
}

// MARK: - ITraffyListViewController
extension TraffyListViewController: TraffyListViewControllerProtocol {
	func showIssueList(viewModel: GetIssueListUseCase.ViewModel) {
		self.viewModels = viewModel.issueListViewModels
		collectionView.reload()
	}
	
	func showSelectedIssue(viewModel: SelectedIssueUseCase.ViewModel) {
		router.openDetail(issue: viewModel.issue)
	}
}

// MARK: - UICollectionViewDataSource
extension TraffyListViewController: UICollectionViewDataSource {
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		viewModels.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let viewModel = viewModels[indexPath.item]
		let identifier = String(describing: TraffyDataListCollectionViewCell.self)
		let cell: TraffyDataListCollectionViewCell = collectionView.dequeueReusableCell(identifier: identifier, indexPath: indexPath)
		cell.configure(viewModel: viewModel)
		return cell
	}
}

// MARK: - UICollectionViewDelegate
extension TraffyListViewController: UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let selectIndex = indexPath.item
		selectIssue(selectedIndex: selectIndex)
	}
}

// MARK: - UICollectionViewDelegateFlowLayout
extension TraffyListViewController: UICollectionViewDelegateFlowLayout {
	func collectionView(_ collectionView: UICollectionView,
						layout collectionViewLayout: UICollectionViewLayout,
						minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		Constants.lineSpcaing
	}
	
	func collectionView(_ collectionView: UICollectionView,
						layout collectionViewLayout: UICollectionViewLayout,
						sizeForItemAt indexPath: IndexPath) -> CGSize {
		let padding = Constants.horizontalPadding + Constants.horizontalPadding + (Constants.lineSpcaing * (Constants.numberOfColumn - 1 ))
		let width = (collectionView.frame.width - padding) / Constants.numberOfColumn
		let height = width + Constants.contentHeight
		return CGSize(width: width, height: height)
	}
	
	func collectionView(_ collectionView: UICollectionView,
						layout collectionViewLayout: UICollectionViewLayout,
						insetForSectionAt section: Int) -> UIEdgeInsets {
		UIEdgeInsets(top: .zero, left: Constants.horizontalPadding, bottom: .zero, right: Constants.horizontalPadding)
	}
}

// MARK: - Private
private extension TraffyListViewController {
	
	func setupUI() {
		title = String(describing: TraffyListViewController.self)
		view.backgroundColor = .white
	}
	
	func setupCollectionView() {
		let bundle = Bundle(for: TraffyDataListCollectionViewCell.self)
		let identifier = String(describing: TraffyDataListCollectionViewCell.self)
		let nib = UINib(nibName: identifier, bundle: bundle)
		
		collectionView.register(nib, forCellWithReuseIdentifier: identifier)
		
		collectionView.delegate = self
		collectionView.dataSource = self
	}
}
