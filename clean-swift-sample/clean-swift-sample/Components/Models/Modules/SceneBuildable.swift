//
//  SceneBuildable.swift
//  clean-swift-sample
//
//  Created by Taratorn Deachboon on 28/7/2565 BE.
//

import UIKit

enum SceneBuilderException: Error {
	case parameterNotFound
	case moduleNotFound
}

protocol SceneBuildable {
	static func build(parameter: [String: Any?]?) throws -> UIViewController
}
