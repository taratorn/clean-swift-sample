//
//  TraffyIssueListEntityBuilder.swift
//  clean-swift-sampleTests
//
//  Created by Taratorn Deachboon on 18/8/2565 BE.
//

@testable import clean_swift_sample

struct TraffyIssueListEntityBuilder {
	typealias Output = [TraffyIssueEntity]
}

extension TraffyIssueListEntityBuilder: IMockBuilder {
	static func build(parameter: [String : Any?]?) -> [TraffyIssueEntity] {
		[ TraffyIssueEntity() ]
	}
}
